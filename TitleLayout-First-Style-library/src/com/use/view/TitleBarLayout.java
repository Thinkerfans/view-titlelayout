package com.use.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.use.tools.UITools;

public class TitleBarLayout extends LinearLayout implements OnItemClickListener {
	private static final String TAG = "TitleBarLayout";
	private static final int DEFAULT_BANK_ICON = R.drawable.default_title_back_icon;
	private static final int DEFAULT_ICON = R.drawable.default_title_back_icon;
	private static final int DEFAULT_NAVI_ICON = R.drawable.default_navi_more_icon;
	private static final int DEFAULT_TEXT_COLOR = Color.WHITE;

	private ImageView mBackIndicator;
	private ImageView mIconIv;
	private LinearLayout mHomeView;
	private TextView mTitleTextTv;
	private ImageView mNaviIv;
	private LinearLayout mNaviLayout;
	private PopupWindow mPopupWindow;

	private ArrayList<NaviItem> mNaviList;
	private NaviListAdapter mAdapter;
	private TitleBarListener mListener;

	private List<ActionItem> mActions;
	private LinearLayout mActionLayout;
	public interface ActionListener{
		void onActionPerformed(int id);
	};
	private ActionListener mActionListener;

	private static final Handler sHandler = new Handler(){
		public void handleMessage(Message msg) {
			TitleBarLayout obj = (TitleBarLayout) msg.obj;
			if(obj.mActionListener != null){
				obj.mActionListener.onActionPerformed(msg.what);
			}
		};
	};

	public interface TitleBarListener {
		void onBackClick();
		void onNaviItemClick(int position);
	}

	public TitleBarLayout(Context context) {
		super(context);
		init();
	}

	public TitleBarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		setGravity(Gravity.CENTER_VERTICAL);
		addView(makeHomeView());
		addView(makeEmptyView());
		mActionLayout = makeActionLayout();
		addView(mActionLayout);
		addView(makeNaviLayout());
		setBackgroundColor(0xFF394A59);
	}

	private View makeHomeView(){
		mHomeView = new LinearLayout(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);
		mHomeView.setLayoutParams(lp);
		mHomeView.setOrientation(HORIZONTAL);
		mHomeView.setGravity(Gravity.CENTER_VERTICAL);
		mHomeView.addView(makeBackIv());
		mHomeView.addView(makeIconIv());
		mHomeView.addView(makeTitleTextTv());
		mHomeView.setBackgroundResource(R.drawable.action_item_bg);
		return mHomeView;
	}

	private ImageView makeBackIv() {
		mBackIndicator = new ImageView(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		lp.rightMargin = UITools.dip2px(getContext(), 5);
		lp.leftMargin = UITools.dip2px(getContext(), 8);
		mBackIndicator.setLayoutParams(lp);
		mBackIndicator.setScaleType(ScaleType.FIT_XY);
		mBackIndicator.setDuplicateParentStateEnabled(true);
		mBackIndicator.setImageResource(DEFAULT_BANK_ICON);
		return mBackIndicator;
	}

	private ImageView makeIconIv() {
		mIconIv = new ImageView(getContext());
		mIconIv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		mIconIv.setScaleType(ScaleType.FIT_XY);
		mIconIv.setImageResource(DEFAULT_ICON);
		mIconIv.setDuplicateParentStateEnabled(true);
		mIconIv.setVisibility(View.GONE);
		return mIconIv;
	}

	private TextView makeTitleTextTv() {
		mTitleTextTv = new ScrollingTextView(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		lp.leftMargin = UITools.dip2px(getContext(), 6);
		mTitleTextTv.setPadding(0,
				0,
				UITools.dip2px(getContext(), 10),
				0);
		mTitleTextTv.setLayoutParams(lp);
		mTitleTextTv.setTextSize(18);
		mTitleTextTv.setTextColor(DEFAULT_TEXT_COLOR);
		return mTitleTextTv;
	}

	private View makeEmptyView() {
		View view = new View(getContext());
		LayoutParams lp = new LayoutParams(0, 1);
		lp.weight = 1;
		view.setLayoutParams(lp);
		return view;
	}
	private static class ActionItem{
		public ActionItem(String title, int icon, int id) {
			this.title = title;
			this.icon = icon;
			this.id = id;
		}
		String title;
		int icon;
		int id;
	}

	private LinearLayout makeActionLayout() {
		LinearLayout v = new LinearLayout(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);
		v.setLayoutParams(lp);
		v.setOrientation(LinearLayout.HORIZONTAL);
		return v;
	}


	private View createActionView(final ActionItem ai){
		View view = null;
		if(!TextUtils.isEmpty(ai.title)){
			Button btn = new Button(getContext());
			btn.setText(ai.title);
			btn.setTextSize(18);
			btn.setTextColor(Color.WHITE);
			view = btn;
		}else{
			ImageView iv = new ImageView(getContext());
			iv.setImageResource(ai.icon);
			view = iv;
		}
		view.setPadding(
				UITools.dip2px(getContext(), 15),
				0,
				UITools.dip2px(getContext(), 15),
				0);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);
		view.setLayoutParams(lp);
		view.setBackgroundResource(R.drawable.action_item_bg);
		view.setClickable(true);
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Message msg = sHandler.obtainMessage(ai.id, TitleBarLayout.this);
				msg.sendToTarget();
			}
		});
		return view;
	}

	private void invalidActions(){
		mActionLayout.removeAllViews();
		if(mActions == null){
			return;
		}
		for(ActionItem ai : mActions){
			mActionLayout.addView(createActionView(ai));
		}
	}

	public void setActionListener(ActionListener listener){
		mActionListener = listener;
	}

	private void addAction(String title, int icon, int id){
		if(mActions == null){
			mActions = new ArrayList<ActionItem>();
		}
		mActions.add(new ActionItem(title, icon, id));
		invalidActions();
	}
	
	public void setTextAction(int id, String title){
		int index = -1;
		for(int i = 0; i < mActions.size(); i++){
			ActionItem ai = mActions.get(i);
			if(ai.id == id){
				index = i;
				break;
			}
		}
		if(index == -1){
			Log.d(TAG, "id not found");
			return;
		}
		try {
			TextView tv = (TextView) mActionLayout.getChildAt(index);
			tv.setText(title);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setIconAction(int id, int icon){
		int index = -1;
		for(int i = 0; i < mActions.size(); i++){
			ActionItem ai = mActions.get(i);
			if(ai.id == id){
				index = i;
				break;
			}
		}
		if(index == -1){
			Log.d(TAG, "id not found");
			return;
		}
		try {
			ImageView iv = (ImageView) mActionLayout.getChildAt(index);
			iv.setImageResource(icon);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void addTextAction(String title, int id){
		if(TextUtils.isEmpty(title)){
			throw new IllegalArgumentException("title cannot be empty!");
		}
		addAction(title, 0, id);
	}

	public void addIconAction(int icon, int id){
		if(icon == 0){
			throw new IllegalArgumentException("icon cannot be empty!");
		}
		addAction(null, icon, id);
	}

	public void removeActions(){
		mActions = null;
		invalidActions();
	}


	private ImageView makeNaviIv() {
		mNaviIv = new ImageView(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);
		lp.leftMargin = UITools.dip2px(getContext(), 20);
		lp.rightMargin = UITools.dip2px(getContext(), 20);
		mNaviIv.setLayoutParams(lp);
		mNaviIv.setScaleType(ScaleType.CENTER);
		mNaviIv.setImageResource(DEFAULT_NAVI_ICON);
		mNaviIv.setDuplicateParentStateEnabled(true);
		return mNaviIv;
	}

	private LinearLayout makeNaviLayout() {
		mNaviLayout = new LinearLayout(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);
		mNaviLayout.setLayoutParams(lp);
		mNaviLayout.setBackgroundResource(R.drawable.action_item_bg);
		mNaviLayout.addView(makeNaviIv());
		mNaviLayout.setOnClickListener(new ViewClickListener());
		return mNaviLayout;
	}

	private void showPopupWindow() {
		if (mPopupWindow == null) {
			mPopupWindow = new PopupWindow(getContext());
			mPopupWindow.setOutsideTouchable(true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setWidth(UITools.dip2px(getContext(), 150));
			mPopupWindow.setHeight(LayoutParams.WRAP_CONTENT);
			mPopupWindow
			.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
			mPopupWindow.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.title_navi_background));
			mPopupWindow.setContentView(createListView());
		}

		mPopupWindow.showAsDropDown(mNaviLayout, 0, 0);
	}

	private ListView createListView() {
		ListView listView = new DropDownListView(getContext(), false);
		mAdapter = new NaviListAdapter(getContext(), mNaviList);
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);
		listView.setFocusable(true);
		listView.setFocusableInTouchMode(true);
		listView.setDivider(getResources().getDrawable(
				R.drawable.popupwindow_divider_line));
		listView.setLayoutParams(new AbsListView.LayoutParams(
				AbsListView.LayoutParams.WRAP_CONTENT,
				AbsListView.LayoutParams.WRAP_CONTENT));
		return listView;
	}

	public void setTitleIcon(int resId) {	
		mIconIv.setImageResource(resId);
	}
	public void showTitleIcon(boolean show){
		mIconIv.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	public void showBackIndicator(boolean isShow) {
		if (isShow) {
			mBackIndicator.setVisibility(View.VISIBLE);
			mHomeView.setOnClickListener(new ViewClickListener());
		} else {
			mBackIndicator.setVisibility(View.GONE);
			resetTitlePosition();
			mHomeView.setOnClickListener(null);
			mHomeView.setClickable(false);
		}
	}

	/**重新设置titleView的位置
	 * 
	 * **/
	private void resetTitlePosition(){
		LayoutParams lp = (LayoutParams) mTitleTextTv.getLayoutParams();
		if(lp!=null){
			lp.leftMargin = UITools.px2dip(getContext(), 36);
		}
	}
	
	public void setTitleText(String text) {
		mTitleTextTv.setText(text);
	}

	public void setTitleText(int resId) {
		mTitleTextTv.setText(resId);
	}

	public void showNavigation(boolean isShow) {
		if (isShow) {
			mNaviLayout.setVisibility(View.VISIBLE);
		} else {
			mNaviLayout.setVisibility(View.GONE);
		}
	}

	public void setNavigationList(ArrayList<NaviItem> itemList) {
		mNaviList = itemList;
	}

	public void addNavigationItem(int iconId, String itemName) {
		if (mNaviList == null) {
			mNaviList = new ArrayList<NaviItem>();
		}
		mNaviList.add(new NaviItem(iconId, itemName));
	}

	public void setTitleBarListener(TitleBarListener l) {
		mListener = l;
	}

	private class ViewClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (v == mHomeView) {
				if (mListener != null) {
					mListener.onBackClick();
				}
			} else if (v == mNaviLayout) {
				if (mNaviList != null && mNaviList.size() > 0) {
					showPopupWindow();
				}
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mPopupWindow.dismiss();
		if (mListener != null) {
			mListener.onNaviItemClick(arg2);
		}
	}

	private static class NaviListAdapter extends BaseAdapter {
		private List<NaviItem> mNaviList;
		private LayoutInflater mInflater;

		public NaviListAdapter(Context context, List<NaviItem> list) {
			super();
			mInflater = LayoutInflater.from(context);
			mNaviList = list;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;

			if (convertView == null) {
				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.navigation_list_item,
						null);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.navigation_list_item_icon);
				holder.name = (TextView) convertView
						.findViewById(R.id.navigation_list_item_name);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			NaviItem item = mNaviList.get(position);
			holder.icon.setImageResource(item.getIconId());
			holder.name.setText(item.getName());
			return convertView;
		}

		private class ViewHolder {
			ImageView icon;
			TextView name;
		}

		@Override
		public int getCount() {
			return mNaviList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}

	class DropDownListView extends ListView {
		private boolean mListSelectionHidden;

		private boolean mHijackFocus;

		public DropDownListView(Context context, boolean hijackFocus) {
			super(context, null);
			mHijackFocus = hijackFocus;
			setCacheColorHint(0);
			// Transparent, since the background drawable
			// could be anything.
			setSelector(R.drawable.transparent);
		}

		@Override
		public boolean isInTouchMode() {
			// WARNING: Please read the comment where mListSelectionHidden is
			// declared
			return (mHijackFocus && mListSelectionHidden)
					|| super.isInTouchMode();
		}

		@Override
		public boolean hasWindowFocus() {
			return mHijackFocus || super.hasWindowFocus();
		}

		@Override
		public boolean isFocused() {
			return mHijackFocus || super.isFocused();
		}

		@Override
		public boolean hasFocus() {
			return mHijackFocus || super.hasFocus();
		}
	}

	public static class NaviItem {

		private int iconId;
		private String name;

		public NaviItem() {

		}

		public NaviItem(int iconId, String name) {
			this.iconId = iconId;
			this.name = name;
		}

		public int getIconId() {
			return iconId;
		}

		public void setIconId(int iconId) {
			this.iconId = iconId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}
}
