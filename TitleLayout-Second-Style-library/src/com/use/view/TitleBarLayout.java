package com.use.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TitleBarLayout extends RelativeLayout implements OnClickListener {

	private ImageView mBack;
	private ImageView mForward;
	private TextView mtitleTv;

	private RelativeLayout mtitleLayout;
	private TitleBarListener mListener;

	public interface TitleBarListener {

		void onBackClick();

		void onForwardClick();
	}
	
	public TitleBarLayout(Context context){
		this(context, null);
	}

	public TitleBarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout(context);
		initTitle();

	}

	private void initTitle() {
		mBack = (ImageView) findViewById(R.id.title_back);
		mForward = (ImageView) findViewById(R.id.title_forward);
		mtitleTv = (TextView) findViewById(R.id.title_text);
		mBack.setOnClickListener(this);
		mForward.setOnClickListener(this);
	}

	private void initLayout(Context context) {
		mtitleLayout = (RelativeLayout) LayoutInflater.from(context).inflate(
				R.layout.title_layout, null);
		addView(mtitleLayout);
	}

	public void setOnTitleBarListener(TitleBarListener l) {
		mListener = l;
	}

	
	
	public void setTitleText(String s) {
		if (TextUtils.isEmpty(s)) {
			throw new IllegalArgumentException("can not set empty title text!");
		}
		mtitleTv.setText(s);
	}

	public void setTitleText(int resId) {
		if (0 == resId) {
			throw new IllegalArgumentException("can not set empty title text!");
		}
		mtitleTv.setText(resId);
	}

	public void setBackIcon(int resId) {
		if (0 == resId) {
			throw new IllegalArgumentException("can not set res id 0!");
		}
		mBack.setImageResource(resId);
	}

	public void setBackIcon(Drawable res) {
		if (null == res) {
			throw new IllegalArgumentException("can not set drawable null!");
		}
		mBack.setImageDrawable(res);

	}

	public void setForwardIcon(int resId) {
		if (0 == resId) {
			throw new IllegalArgumentException("can not set res id 0!");
		}
		mForward.setImageResource(resId);
	}

	public void setForwardIcon(Drawable res) {
		if (null == res) {
			throw new IllegalArgumentException("can not set drawable null!");
		}
		mForward.setImageDrawable(res);
	}

	public void showBackView(boolean visiable) {
		if (visiable) {
			mBack.setVisibility(View.VISIBLE);
		} else {
			mBack.setVisibility(View.INVISIBLE);
		}
	}

	public void showForwardView(boolean visiable) {
		if (visiable) {
			mForward.setVisibility(View.VISIBLE);
		} else {
			mForward.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		if (null != mListener) {
			if (R.id.title_back == v.getId()) {
				mListener.onBackClick();
			} else if (R.id.title_forward == v.getId()) {
				mListener.onForwardClick();
			}
		}

	}

}
