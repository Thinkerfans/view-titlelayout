package com.layout.styles.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.use.view.TitleBarLayout;
import com.use.view.TitleBarLayout.TitleBarListener;

public class StyleSecondTestActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.style_second_test_layout);
        initTitle();
        Button startProgress = (Button) findViewById(R.id.show_back_view);
        startProgress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                title.showBackView(true);
            }
        });
        
        Button stopProgress = (Button) findViewById(R.id.show_forward_view);
        stopProgress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                title.showForwardView(true);

            }
        });

        Button removeActions = (Button) findViewById(R.id.set_title_text);
        removeActions.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setTitleText("1111111111111111111111111111111111111111111111111111");
            }
        });

        Button addAction = (Button) findViewById(R.id.add_listener);
        addAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	
            	title.setOnTitleBarListener(new TitleBarListener() {
					
					@Override
					public void onForwardClick() {
						// TODO Auto-generated method stub
						 Toast.makeText(StyleSecondTestActivity.this, "onForwardClick", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					public void onBackClick() {
						// TODO Auto-generated method stub
						 Toast.makeText(StyleSecondTestActivity.this, "onBackClick", Toast.LENGTH_SHORT).show();
					}
				});
 
            }
        });

        Button removeAction = (Button) findViewById(R.id.remove_listener);
        removeAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	title.setOnTitleBarListener(null);
            }
        });

        Button removeShareAction = (Button) findViewById(R.id.hide_back_view);
        removeShareAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	title.showBackView(false);
            }
        });
        
       
       
    }
    
    TitleBarLayout title ;
    
    private void  initTitle(){
    	
        title = (TitleBarLayout) findViewById(R.id.title_layout);
    	title.showBackView(false);
    	title.showForwardView(false);
    	title.setTitleText("Title ");
    	
    }
 
}